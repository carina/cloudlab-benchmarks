#!/bin/bash

cd ~

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)

#######################
### Parse Arguments ###
#######################
iodepth=0
benchname=""
device=""
while getopts ":n:b:d:" opt; do
    case ${opt} in
        n )
            iodepth=$OPTARG
            ;;
        b )
            benchname=$OPTARG
            ;;
        d )
            device=$OPTARG
            ;;
    esac
done

#####################
### Failure Modes ###
#####################
# iodepth
if [ $iodepth -lt 1 ]; then
    echo "Bad iodepth Setting (iodepth=$iodepth)."
    exit 1
fi

# Benchmark Name
if [[ ! "$benchname" =~ ^(read|randread|write|randwrite)$ ]]; then
    echo "Bad Benchmark Name (benchname=$benchname)."
    exit 1
fi

# Device Name
if [[ $device ==  "" ]]; then
    echo "Bad Device Name (device=$device)."
    exit 1
fi

###########
### FIO ###
###########
fio_version=$(fio -v)
# Huge hardcoded FIO header, this is the worst...
fioheader="terse_version,fio_version,jobname,groupid,error,READ_kb,READ_bandwidth,READ_IOPS,READ_runtime,READ_Slat_min,READ_Slat_max,READ_Slat_mean,READ_Slat_dev,READ_Clat_max,READ_Clat_min,READ_Clat_mean,READ_Clat_dev,READ_clat_pct01,READ_clat_pct02,READ_clat_pct03,READ_clat_pct04,READ_clat_pct05,READ_clat_pct06,READ_clat_pct07,READ_clat_pct08,READ_clat_pct09,READ_clat_pct10,READ_clat_pct11,READ_clat_pct12,READ_clat_pct13,READ_clat_pct14,READ_clat_pct15,READ_clat_pct16,READ_clat_pct17,READ_clat_pct18,READ_clat_pct19,READ_clat_pct20,READ_tlat_min,READ_lat_max,READ_lat_mean,READ_lat_dev,READ_bw_min,READ_bw_max,READ_bw_agg_pct,READ_bw_mean,READ_bw_dev,WRITE_kb,WRITE_bandwidth,WRITE_IOPS,WRITE_runtime,WRITE_Slat_min,WRITE_Slat_max,WRITE_Slat_mean,WRITE_Slat_dev,WRITE_Clat_max,WRITE_Clat_min,WRITE_Clat_mean,WRITE_Clat_dev,WRITE_clat_pct01,WRITE_clat_pct02,WRITE_clat_pct03,WRITE_clat_pct04,WRITE_clat_pct05,WRITE_clat_pct06,WRITE_clat_pct07,WRITE_clat_pct08,WRITE_clat_pct09,WRITE_clat_pct10,WRITE_clat_pct11,WRITE_clat_pct12,WRITE_clat_pct13,WRITE_clat_pct14,WRITE_clat_pct15,WRITE_clat_pct16,WRITE_clat_pct17,WRITE_clat_pct18,WRITE_clat_pct19,WRITE_clat_pct20,WRITE_tlat_min,WRITE_lat_max,WRITE_lat_mean,WRITE_lat_dev,WRITE_bw_min,WRITE_bw_max,WRITE_bw_agg_pct,WRITE_bw_mean,WRITE_bw_dev,CPU_user,CPU_sys,CPU_csw,CPU_mjf,PU_minf,iodepth_1,iodepth_2,iodepth_4,iodepth_8,iodepth_16,iodepth_32,iodepth_64,lat_2us,lat_4us,lat_10us,lat_20us,lat_50us,lat_100us,lat_250us,lat_500us,lat_750us,lat_1000us,lat_2ms,lat_4ms,lat_10ms,lat_20ms,lat_50ms,lat_100ms,lat_250ms,lat_500ms,lat_750ms,lat_1000ms,lat_2000ms,lat_over_2000ms,disk_name,disk_read_iops,disk_write_iops,disk_read_merges,disk_write_merges,disk_read_ticks,write_ticks,disk_queue_time,disk_utilization,device,iod\n"
direct=1
numjobs=1
ioengine="libaio"
blocksize="4k"
size="10G"
# timeout is 12 minutes
timeout=720
disk="/dev/$device"

echo -n "Running fio on $disk with operation $benchname and iodepth $iodepth - "
date
name=""
output="fio_${benchname}_io${iodepth}_${device}.csv"
if [[ "$benchname" =~ ^(write|randwrite)$ ]]; then
    sudo blkdiscard $disk
fi
sudo fio --name=$benchname --filename=$disk --bs=$blocksize --size=$size --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$benchname --minimal --output=$output
sed -i "1s@\$@;$disk;$iodepth@" ~/$output
sed -i 's/\;/\,/g' ~/$output
sed -i "1s/^/$fioheader/" ~/$output
if [ $iodepth -gt 1 ]; then
    output="fio_info.csv"
    if [[ ! -f "$output" ]]; then
        echo "run_uuid,timestamp,nodeid,nodeuuid,fio_version,fio_size,fio_iodepth,fio_direct,fio_numjobs,fio_ioengine,fio_blocksize,fio_timeout" > ~/$output
        echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$fio_version,$size,$iodepth,$direct,$numjobs,$ioengine,$blocksize,$timeout" >> ~/$output
    fi
fi