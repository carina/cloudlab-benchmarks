#!/bin/bash

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)

###########################
### Network Information ###
###########################
echo -n "Getting Network Information - "
date
# Grab the physical interface serving the VLAN
vlan_to_link=$(sudo ip link | grep vlan | awk '{print $2}' | tr '@' ' ' | tr -d ':')

# Parse VLAN info
vlan_name=$(echo $vlan_to_link | awk '{print $1}')
vlan_ip=$(sudo ip addr show dev $vlan_name | grep 'inet' | grep -v 'inet6' | awk '{print $2}' |  cut -d"/" -f 1)
vlan_hwaddr=$(sudo ip addr show dev $vlan_name | grep 'link/ether' | awk '{ print $2 }')
vlan_driver=$(sudo ethtool -i $vlan_name | grep driver | awk '{print substr($0, index($0, $2))}')
vlan_driver_ver=$(sudo ethtool -i $vlan_name | grep version | grep -v -E 'firmware|rom' | awk '{print substr($0, index($0, $2))}')

# Parse interface info
if_name=$(echo $vlan_to_link | awk '{print $2}')
if_hwaddr=$(sudo ip addr show dev $if_name | grep 'link/ether' | awk '{ print $2 }')
if_hwinfo=$(sudo lshw -class network -businfo | grep $if_name | awk '{print substr($0, index($0, $4))}')
if_speed=$(sudo ethtool $if_name | grep Speed | awk '{print $2}')
if_duplex=$(sudo ethtool $if_name | grep Duplex | awk '{print $2}')
if_port_type=$(sudo ethtool $if_name | grep Port | awk '{print substr($0, index($0, $2))}')
if_driver=$(sudo ethtool -i $if_name | grep driver | awk '{print substr($0, index($0, $2))}')
if_driver_ver=$(sudo ethtool -i $if_name | grep version | grep -v -E 'firmware|rom' | awk '{print substr($0, index($0, $2))}')
if_bus_location=$(sudo ethtool -i $if_name | grep bus | awk '{print $2}')

echo "run_uuid,timestamp,nodeid,nodeuuid,vlan_name,vlan_ip,vlan_hwaddr,vlan_driver,vlan_driver_ver,if_name,if_hwaddr,if_hwinfo,if_speed,if_duplex,if_port_type,if_driver,if_driver_ver,if_bus_location" > ~/net_info.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$vlan_name,$vlan_ip,$vlan_hwaddr,$vlan_driver,$vlan_driver_ver,$if_name,$if_hwaddr,$if_hwinfo,$if_speed,$if_duplex,$if_port_type,$if_driver,$if_driver_ver,$if_bus_location" >> ~/net_info.csv