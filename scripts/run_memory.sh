#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
nsockets=1
arch=$(uname -m)
if [ ${arch} == 'x86_64' ]; then
    nsockets=$(cat /proc/cpuinfo | grep "physical id" | sort -n | uniq | wc -l)
fi
nthreads_total=$(nproc --all)
nthreads=$((nthreads_total/nsockets))

#######################
### Parse Arguments ###
#######################
socket_num=-1
dvfs=-1
benchname=""
argstring=""
threading=""
while getopts ":n:d:b:a:t:" opt; do
    case ${opt} in
        n )
            socket_num=$OPTARG
            ;;
        d )
            dvfs=$OPTARG
            ;;
        b )
            benchname=$OPTARG
            ;;
        a )
            argstring="$OPTARG"
            ;;
        t )
            threading=$OPTARG
            ;;
    esac
done

#####################
### Failure Modes ###
#####################
# Socket Number
if [ $socket_num -lt 0 ] || [ $socket_num -ge $nsockets ]; then
    echo "Bad Socket Number (socket_num=$socket_num)."
    exit 1
fi

# DVFS
if [[ ! "$dvfs" =~ ^(0|1)$ ]]; then
    echo "Bad DVFS Setting (dvfs=$dvfs)."
    exit 1
fi

# Benchmark Name
if [[ ! "$benchname" =~ ^(stream|membench)$ ]]; then
    echo "Bad Benchmark Name (benchname=$benchname)."
    exit 1
fi

# Argstring
if [[ $argstring ==  "" ]] && [[ "$benchname" == "membench" ]]; then
    echo "Bad Argument String (argstring=$argstring)."
    exit 1
fi

# Threading
if [[ ! "$threading" =~ ^(ST|MT)$ ]] && [[ "$benchname" == "stream" ]]; then
    echo "Bad Threading Mode (threading=$threading)."
    exit 1
fi

###################
### Modify DVFS ###
###################
if [ ${dvfs} == 0 ]; then
    if [ ${arch} == 'x86_64' ] && [ -z $(lscpu | grep "Model name:" | grep -o -m 1 6142 | head -1) ]; then
        # Turn DVFS stuff off, re-run memory experiments
        dvfs_txt="no"
        dvfs_file="nodvfs"
        sudo modprobe msr
        oldgovernor=$(sudo cpufreq-info -p | awk '{print $3}')
        for (( n=0; n<=$((nthreads-1)); n++ ))
        do
            sudo wrmsr -p$n 0x1a0 0x4000850089
            sudo cpufreq-set -c $n -g performance
        done
    else
        echo "Unable to modify DVFS on current platform."
        exit 1
    fi
elif [ ${dvfs} == 1 ]; then
    dvfs_txt="yes"
    dvfs_file="dvfs"
fi

#####################
### Run Benchmark ###
#####################
cd ..
if [[ $benchname == "stream" ]]; then
    with_OMP=''
    if [[ "$threading" == "MT" ]]; then
        with_OMP='-o'
    fi
    filename="stream_out_socket${socket_num}_${dvfs_file}.csv"
    cd STREAM-separate
    echo -n "Running STREAM (dvfs $dvfs_txt, socket $socket_num) - "
    date
    numactl -N $socket_num ./streamc $with_OMP
    old_filename="stream_out.csv"
elif [[ $benchname == "membench" ]]; then
    filename="membench_out_socket${socket_num}_${dvfs_file}.csv"
    cd membench-separate
    echo -n "Running membench (dvfs $dvfs_txt, socket $socket_num, argstring $argstring) - "
    date
    numactl -N $socket_num ./memory_profiler -$argstring
    old_filename="memory_profiler_out.csv"
fi

#########################
### File Manipulation ###
#########################
if [[ ! -f ~/$filename ]]; then
    echo 'run_uuid,timestamp,nodeid,nodeuuid,socket_num,dvfs,omp_nthreads_used,units' > ~/$filename
    echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$socket_num,$dvfs_txt,$nthreads,MB/s" >> ~/$filename
fi
paste -d "" ~/$filename $old_filename > temp.csv
mv temp.csv ~/$filename
rm -f $old_filename


###################
### Revert DVFS ###
###################
if [ ${dvfs} == 0 ]; then
    for (( n=0; n<=$((nthreads-1)); n++ ))
    do
        sudo wrmsr -p$n 0x1a0 0x850089
        sudo cpufreq-set -c $n -g $oldgovernor
    done
fi