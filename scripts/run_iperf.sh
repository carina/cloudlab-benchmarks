#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
net_server=192.168.1.100

#######################
### Parse Arguments ###
#######################
direction=""
while getopts ":d:" opt; do
    case ${opt} in
        d )
            direction=$OPTARG
            ;;
    esac
done

#####################
### Failure Modes ###
#####################
# Direction
if [[ ! "$direction" =~ ^(normal|reversed)$ ]]; then
    echo "Bad Direction (direction=$direction)."
    exit 1
fi

#########################
### Network Bandwidth ###
#########################
# iperf3 client -> server first
iperf_omit=1
iperf_time=60
if [ ! -z $(egrep 'hp|amd' /var/emulab/boot/nodeid) ]; then
    # 25Gbps links seem to like higher buffer size
    iperf_buff_size=1M
else
    # Default buffer size
    iperf_buff_size=128k
fi
direction_arg=""
if [[ "$direction" == "reversed" ]]; then
    direction_arg="-R"
fi
echo -n "Running iperf3 $direction - "
date

iperf_status=1
lock_status=2
can_run=1
curr_timeout=0
max_timeout=300

# Send a lock request to the Arbiter
until python3 ../net_client.py -i $net_server -p 65432 -o request
do
    lock_status=$?
    if [ $lock_status -eq 1 ]; then
        can_run=0
        break
    fi
    sleep 5
    let "curr_timeout+=5"
    if [ "$curr_timeout" -gt "$max_timeout" ]; then
        can_run=0
        break
    fi
done

if [ $can_run -eq 1 ]; then
    iperf3 -V $direction_arg -J -N -O $iperf_omit -t $iperf_time -l $iperf_buff_size -c $net_server > ~/iperf3_$direction.json
    iperf_status=$?
    # Release the lock on the Arbiter
    python3 ../net_client.py -i $net_server -p 65432 -o release
    if [ $iperf_status -ne 0 ]; then
        echo "Execution Failed, cleaning up output..."
        rm ~/iperf3_$direction.json
    fi
fi