#!/bin/bash

######################
### Initialization ###
######################
date -u +%s > ~/timestamp.out
uuidgen > ~/run_uuid.out
mkdir ~/complete
cd ~/complete
echo "INCOMPLETE" > run_complete
nohup python "$(dirname ${BASH_SOURCE[0]})"/../simplehttp.py 0.0.0.0:8000 > ~/http.log 2> ~/http_error.log &