#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Initial Variables ###
#########################
timestamp=$(cat ~/timestamp.out)
run_uuid=$(cat ~/run_uuid.out)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
vlan_to_link=$(sudo ip link | grep vlan | awk '{print $2}' | tr '@' ' ' | tr -d ':')
if_name=$(echo $vlan_to_link | awk '{print $2}')
if_hwinfo=$(sudo lshw -class network -businfo | grep $if_name | awk '{print substr($0, index($0, $4))}')

########################
### Install Packages ###
########################
echo -n "Installing Packages - "
date
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
# Env Info packages
sudo apt-get install hwinfo numactl -y
# IPutils build dependencies
sudo apt-get install libcap-dev libidn2-0-dev nettle-dev libnuma-dev -y
# NPB build dependencies
sudo apt-get install gfortran -y
# DVFS dependencies
sudo apt-get install msr-tools cpufrequtils -y
# fio and dependencies
sudo apt-get install fio gdisk -y
# iperf3
sudo apt-get install iperf3 -y

########################
### Build Benchmarks ###
########################
# Build ping from source
echo -n "Building ping - "
date
cd ../iputils-ns
make
cd ..

# Build STREAM from source
echo -n "Building STREAM - "
date
stream_ntimes=200
stream_array_size=50000000
stream_offset=0
stream_type=double
stream_optimization=O2
cd ./STREAM-separate
make clean
make NTIMES=$stream_ntimes STREAM_ARRAY_SIZE=$stream_array_size OFFSET=$stream_offset STREAM_TYPE=$stream_type OPT=$stream_optimization
echo "run_uuid,timestamp,nodeid,nodeuuid,stream_ntimes,stream_array_size,stream_offset,stream_type,stream_optimization" > ~/stream_info.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$stream_ntimes,$stream_array_size,$stream_offset,$stream_type,$stream_optimization" >> ~/stream_info.csv
cd ..

# Build membench from source
echo -n "Building membench - "
date
nthreads=$(nproc --all)
membench_samples=20
membench_times=10
membench_size=$(python3 -c "multiple=$nthreads * 32; list = [n for n in range(1024**3, 1024**3 + multiple) if n % multiple == 0]; print(str(list[0]) + 'LL')")
membench_optimization=O3
cd ./membench-separate
make clean
make SAMPLES=$membench_samples TIMES=$membench_times SIZE=$membench_size OPT=$membench_optimization
echo "run_uuid,timestamp,nodeid,nodeuuid,membench_samples,membench_times,membench_size,membench_optimization" > ~/membench_info.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$membench_samples,$membench_times,$membench_size,$membench_optimization" >> ~/membench_info.csv
cd ..

# Build NPB-CPU from source (ST)
echo -n "Building NPB CPU (ST) - "
date
cd ./NPB-CPUTests
cp config/make-ST.def config/make.def
cp config/suite-ST.def config/suite.def
rm -f bin/*
make clean
make suite
cd bin
mv bt.W.x bt.ST
mv cg.A.x cg.ST
mv ep.W.x ep.ST
mv ft.A.x ft.ST
mv is.B.x is.ST
mv lu.W.x lu.ST
mv mg.A.x mg.ST
mv sp.W.x sp.ST
mv ua.W.x ua.ST
cd ..

# Build NPB-CPU from source (MT)
echo -n "Building NPB CPU (MT) - "
date
cp config/make-MT.def config/make.def
total_mem=$(sudo hwinfo --memory | grep "Memory Size" | awk '{print $3 $4}')
if [[ $(echo $total_mem | cut -d"G" -f 1) -lt 20 ]]; then
    cp config/suite-MT-lowmem.def config/suite.def
else
    cp config/suite-MT.def config/suite.def
fi
make suite
cd bin
mv bt.A.x bt.MT
mv cg.B.x cg.MT
mv ep.B.x ep.MT
mv ft.B.x ft.MT
if [[ ! $(echo $total_mem | cut -d"G" -f 1) -lt 20 ]]; then
    mv is.D.x is.MT
fi
mv lu.A.x lu.MT
mv mg.C.x mg.MT
mv sp.A.x sp.MT
mv ua.A.x ua.MT
cd ../..

##############################
### Build Extra Benchmarks ###
##############################
# Build SLANG-probed from source
if [[ ! -z $(echo ${if_hwinfo} | grep 'ConnectX') ]]; then
    # SLANG-probed build dependencies
    sudo apt-get install libxml2-dev pkg-config -y
    # Build SLANG-probed from source
    echo -n "Building SLANG-probed - "
    date
    cd ./SLANG-probed
    autoreconf -i
    ./configure
    make
    cd ..
fi